# for reading variables from .env
import os
from dotenv import load_dotenv, find_dotenv

from flask import Flask

def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY = os.environ.get("SECRET_KEY"),
        DATABASE   = os.path.join(app.instance_path, os.environ.get("SQLITE_FILE")),
        PAGE_NAME  = os.environ.get("PAGE_NAME"),
        TMP_FILE   = os.environ.get("TMP_FILE"),
        
        NC_ADDR = os.environ.get("NC_ADDR"),
        NC_PASS = os.environ.get("NC_PASS"),
        
        SESSION_COOKIE_SAMESITE = 'None',
        SESSION_COOKIE_SECURE = 'True',

        LDAP_HOST = os.environ.get("LDAP_HOST"),
        LDAP_BASEDN = os.environ.get("LDAP_BASEDN"),
        
        LDAP_FILTER = os.environ.get("LDAP_FILTER"),
        LDAP_GROUP_DN = os.environ.get("LDAP_GROUP_DN"),
        LDAP_USER_RDN_ATTR = os.environ.get("LDAP_USER_RDN_ATTR"),
        LDAP_USER_LOGIN_ATTR = os.environ.get("LDAP_USER_LOGIN_ATTR"),
        LDAP_BIND_USER_DN = os.environ.get("LDAP_BIND_USER_DN"),
        LDAP_BIND_USER_PASSWORD = os.environ.get("LDAP_BIND_USER_PASSWORD"),
        
        RECAPTCHA_PUBLIC_KEY = os.environ.get('RECAPTCHA_PUBLIC_KEY'),
        RECAPTCHA_PRIVATE_KEY = os.environ.get('RECAPTCHA_PRIVATE_KEY'),

        SMTP_HOST = os.environ.get('SMTP_HOST'),
        SMTP_PORT = os.environ.get('SMTP_PORT'),
        SMTP_USER = os.environ.get('SMTP_USER'),
        SMTP_PASS = os.environ.get('SMTP_PASS'),
        MAIL_FROM = os.environ.get('MAIL_FROM')
        
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    from flask_wtf.csrf import CSRFProtect
    csrf = CSRFProtect(app)
    csrf.init_app(app)

    from . import db
    db.init_app(app)

    from . import main
    app.register_blueprint(main.bp)

    from flask_bootstrap import Bootstrap
    Bootstrap(app)

    

    return app
