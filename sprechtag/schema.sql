DROP TABLE IF EXISTS contacts;
DROP TABLE IF EXISTS teachers;
DROP TABLE IF EXISTS requests;

CREATE TABLE contacts (
  id           INTEGER PRIMARY KEY AUTOINCREMENT,
  studentClass TEXT NOT NULL,
  studentName  TEXT NOT NULL,
  email        TEXT NOT NULL,
  message      TEXT NOT NULL,
  token        TEXT NOT NULL
);

CREATE TABLE teachers (
  id           INTEGER PRIMARY KEY AUTOINCREMENT,
  sign         TEXT NOT NULL UNIQUE,
  firstname    TEXT NOT NULL,
  lastname     TEXT NOT NULL
);

CREATE TABLE requests (
  id           INTEGER PRIMARY KEY AUTOINCREMENT,
  contactId    TEXT NOT NULL,
  teacherId    TEXT NOT NULL
);
