from flask import current_app

import sqlite3


# update teachers list in database
db = sqlite3.connect(current_app.config['DATABASE'], detect_types=sqlite3.PARSE_DECLTYPES)
db.row_factory = sqlite3.Row

l = ldap.initialize(current_app.config['LDAP_HOST'])
l.protocol_version = ldap.VERSION3
l.simple_bind_s() #binddn, pw)
ldap_result_id = l.search(current_app.config['LDAP_BASEDN'], ldap.SCOPE_SUBTREE, current_app.config['LDAP_FILTER'])
while True:
    result_type, result_data = l.result(ldap_result_id, 0)
    if (result_data == []):
        break
    else:
        if result_type == ldap.RES_SEARCH_ENTRY:
            a, d = result_data[0]
            # filter the admin-, exam- and machine-accounts
            uid = d['uid'][0].decode()
            sn = d['sn'][0].decode()
            givenName = d['givenName'][0].decode()
            try:
                db.execute(
                    "INSERT INTO teachers (nameShort, nameLong) VALUES (?, ?)",
                    (uid, givenName + ' ' + sn)
                )
            except:
                print("dbentry not unique")
db.commit()


for i in db.execute('SELECT * FROM teachers ORDER BY nameShort ASC').fetchall():
    print("LEHRER:", i['nameShort'], i['nameLong'])
