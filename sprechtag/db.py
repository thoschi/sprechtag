import sqlite3
import ldap

import click
from flask import current_app, g
from flask.cli import with_appcontext


def get_db():
    if 'db' not in g:
        g.db = sqlite3.connect(
            current_app.config['DATABASE'],
            detect_types=sqlite3.PARSE_DECLTYPES
        )
        g.db.row_factory = sqlite3.Row

    return g.db


def close_db(e=None):
    db = g.pop('db', None)

    if db is not None:
        db.close()

def init_app(app):
    app.teardown_appcontext(close_db)
    app.cli.add_command(init_db_command)
    app.cli.add_command(update_db_command)

def init_db():
    db = get_db()

    with current_app.open_resource('schema.sql') as f:
        db.executescript(f.read().decode('utf8'))


def update_db():
    db = get_db()
    db.execute('DELETE FROM teachers')
    db.commit()
    
    # get teachers from ldap and write to database
    l = ldap.initialize(current_app.config['LDAP_HOST'])
    l.protocol_version = ldap.VERSION3
    l.simple_bind_s() #binddn, pw)
    ldap_result_id = l.search(current_app.config['LDAP_BASEDN'], ldap.SCOPE_SUBTREE, current_app.config['LDAP_FILTER'])
    while True:
        result_type, result_data = l.result(ldap_result_id, 0)
        if (result_data == []):
            break
        else:
            if result_type == ldap.RES_SEARCH_ENTRY:
                a, d = result_data[0]
                # filter the admin-, exam- and machine-accounts
                uid = d['uid'][0].decode()
                sn = d['sn'][0].decode()
                givenName = d['givenName'][0].decode()
                db.execute(
                    "INSERT INTO teachers (sign, firstname, lastname) VALUES (?, ?, ?)",
                    (uid, givenName, sn)
                )
    db.commit()


@click.command('init-db')
@with_appcontext
def init_db_command():
    """Clear the existing data and create new tables."""
    init_db()
    click.echo('Initialized the database.')

@click.command('update-db')
@with_appcontext
def update_db_command():
    """update the teachers table"""
    update_db()
    click.echo('Updated the database.')
