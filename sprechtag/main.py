from flask import Blueprint, flash, render_template, request, url_for, current_app, send_file, after_this_request
from sprechtag.db import get_db
from flask_wtf import FlaskForm
from wtforms import StringField, SelectMultipleField, TextAreaField, widgets, validators
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo, Length, Regexp
import email_validator
from markupsafe import Markup

# for file handling
import os

import smtplib, ssl
from email.message import EmailMessage

from flask_wtf.recaptcha import RecaptchaField

import gender_guesser.detector as gender

# token generation
import random
import string

# for ods export
from pyexcel_ods3 import save_data

# for nextcloud upload
import nextcloud_client
from collections import OrderedDict

bp = Blueprint('main', __name__, url_prefix='/')

class MultiCheckboxField(SelectMultipleField):
    widget = widgets.ListWidget(prefix_label=False)
    option_widget = widgets.CheckboxInput()

class MessageForm(FlaskForm):
    student_class = StringField('Vorname', validators=[Length(max=120)])
    student_name  = StringField('Nachname', validators=[DataRequired(), Length(max=120)])
    email         = StringField('Email', validators=[DataRequired(), Email(), Length(max=120)])
    email2        = StringField('Wiederholung', validators=[DataRequired(), Email(), Length(max=120)])
    requests      = MultiCheckboxField('Lehrkräfte', choices=[])
    message       = TextAreaField('Sonstige Anmerkungen', validators=[Length(max=255)])
    token         = StringField('Ihr Token', validators=[Length(max=255)])
    # ~ recaptcha     = RecaptchaField()

@bp.route('/', methods=('GET', 'POST'))
def index():

    db = get_db()
    gd = gender.Detector()
    form = MessageForm()
 
    errors = []
    showerrors = False
    showform = True

    # get teachers from db and create choices for selection
    teachers=[]
    for i in db.execute('SELECT * FROM teachers ORDER BY sign ASC').fetchall():
        genders = [ gd.get_gender(fn) for fn in i['firstname'].split('-') ]
        if "male" in genders:
            title = "Herr"
        elif "female" in genders:
            title = "Frau"
        else:
            title = i['firstname']
        teachers.append({'id': i['id'], 'sign': i['sign'], 'title': title, 'firstname': i['firstname'], 'lastname': i['lastname']})

        form.requests.choices.append((i['sign'], title + ' ' + i['lastname']))

    # process filled form
    if request.method == "POST":

        student_class = request.form['student_class']
        student_name  = request.form['student_name']
        requests      = request.form.getlist('requests')
        email         = request.form['email']
        email2        = request.form['email2']
        message       = request.form['message']
        
        # create unique token
        token = ''.join(random.choice(string.ascii_uppercase + string.digits) for i in range(6))
        form.token.data = token
                

        if form.validate_on_submit() and email == email2:
            
            errors = {}
            db = get_db()
            cursor = db.cursor()
            cursor.execute(
                "INSERT INTO contacts (studentClass, studentName, email, message, token) VALUES (?, ?, ?, ?, ?)",
                (student_class, student_name, email, message, token)
            )
            db.commit()
            
            contact_id = cursor.lastrowid
            for r in requests:
                teacher_id = db.execute('SELECT id FROM teachers WHERE sign="' + r + '"').fetchall()[0]['id']
                cursor.execute(
                    "INSERT INTO requests (contactId, teacherId) VALUES (?, ?)",
                    (contact_id, teacher_id)
                )
                db.commit()
            
            # show the summary (not the form)
            showform = False

            # create file from entries
            rows = [["Token", "Klasse", "SchülerIn", "E-Mail", "Nachricht", "Gesprächswünsche"]]
            for c in cursor.execute('SELECT * FROM contacts').fetchall():
                # get requested teacher IDs from database
                teacher_ids = [str(i['teacherId']) for i in cursor.execute('SELECT teacherId FROM requests WHERE contactId="' + str(c['id']) + '"').fetchall()]
                # compose string of requested teachers from teachers list (for website the last names)
                r_web = ', '.join([t['title'] + ' ' + t['lastname'] for t in teachers if str(t['id']) in teacher_ids])
                # compose string of requested teachers from teachers list (for export the signs)
                r_exp = ', '.join([t['sign'].capitalize() for t in teachers if str(t['id']) in teacher_ids])
                rows.append([c['token'], c['studentClass'], c['studentName'], c['email'], c['message'], r_exp])

            data = OrderedDict()
            data.update({"Terminwünsche": rows})
            save_data(current_app.config['TMP_FILE'], data)
            try:
                # upload to nextcloud
                nc = nextcloud_client.Client.from_public_link(current_app.config['NC_ADDR'], folder_password=current_app.config['NC_PASS'])
                nc.drop_file(current_app.config['TMP_FILE'])
            except:
                print("Nextcloud upload failed")
            
            # send confirmation mail
            msg = EmailMessage()
            msg['From'] = current_app.config['MAIL_FROM']
            msg['Subject'] = '[Corvi Elternsprechtag] Eingangsbestätigung'
            msg['To'] = email
            msg.set_content("Sehr geehrte Eltern,\n\nHiermit bestätigen wir den Eingang Ihrer Gesprächswünsche.\nSobald die Pläne zur Verfügung stehen, werden wir Sie erneut benachrichtigen.\nMit freundlichen Grüßen\nCorvinianum Northeim")
            smtp_port = current_app.config['SMTP_PORT']
            smtp_host = current_app.config['SMTP_HOST']
            smtp_user = current_app.config['SMTP_USER']
            smtp_pass = current_app.config['SMTP_PASS']
            context = ssl.create_default_context()
            with smtplib.SMTP(smtp_host, smtp_port) as server:
                server.starttls(context=context)
                server.login(smtp_user, smtp_pass)
                server.send_message(msg)

        else:
            errors = form.errors
            # check for correct mail
            if email != email2 or not form.validate():
                if email != email2:
                    errors['email'] = ['not identical']
                    flash(u'E-Mail-Adressen nicht identisch!', 'error')
                if not form.validate():
                    flash(u'Formulardaten fehlerhaft')

                # pre-fill form and repeat
                form.student_class.data = student_name
                form.student_name.data  = student_class
                form.email.data         = email
                form.email2.data        = email2
                form.message.data       = message

                showerrors = True

    return render_template('index.html', title=current_app.config['PAGE_NAME'], form = form, errors = errors, showerrors = showerrors, showform = showform)

class CheckForm(FlaskForm):
    token         = StringField('Ihr Token', validators=[Length(max=255)])
    # ~ recaptcha     = RecaptchaField()

@bp.route('/check', methods=('GET', 'POST'))
def check():
    form = CheckForm()
 
    errors = []
    
    showerrors = False
    showform = True
    if request.method == "POST":
        print("FORM VALIDATED", request.form)
        # check for the PDF and put it to tmp for download
        showform = False

    return render_template('check.html', title=current_app.config['PAGE_NAME'], form = form, showform = showform)

@bp.route('/download', methods=('GET', 'POST'))
def download():
    if request.method == "GET" and 'token' in request.args:
        print("TOKEN", request.args['token'])
        # todo: write download information to database
        
        @after_this_request
        def remove_file(response):
            os.remove("/tmp/test.txt")
            return response
        
        return send_file("/tmp/test.txt", as_attachment=True)
    else:
        return redirect(url_for('main.check'))
